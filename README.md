The following scripts are tools to help work with and prepare images for Rasbperry PIs

compressor*
	(must be run as root)
	This tool will take an image file name (or 2) and create a compressed image which will
	automatically expand to fill the disk on the first boot.

image_extractor*
	(must be run as root)
	This tool will take an SD card or other media and extract the image from it.

image_mounter*
	(must be run as root)
	This tool accepts the name of an image file and will mount the second partition
	onto /a and the first partition onto /a/boot.

mod_my_pi_sd*
	This tool will make most of the required installation modificatoins to an image.
	It will not install packages, but it does configure SNMP, set up WPA supplicant,
	configure Bluetooth Serial Port, etc.

wpa_supplicant.conf
	This file is used by mod_my_pi and is installed to enable WiFi to connect to the
	desired interfaces.

